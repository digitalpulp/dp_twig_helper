<?php

declare(strict_types=1);

namespace Drupal\dp_twig_helper_acquia_dam_remote_image;

use Drupal\acquia_dam\Entity\ComputedEmbedCodesField;
use Drupal\acquia_dam\Entity\MediaSourceField;
use Drupal\Component\Utility\UrlHelper;
use Drupal\media\MediaInterface;
use Drupal\twig_tweak\TwigTweakExtension;

/**
 * Extends the TwigTweakExtension to provide additional functionality.
 *
 * This decorator enhances existing TwigTweakExtension functionality,
 * with specific handling for "Acquia DAM" media entities.
 */
class TwigTweakExtensionDecorator extends TwigTweakExtension {

  /**
   * Generates a Drupal image render array from a selector.
   *
   *   This method supports "image" file entity and "Acquia DAM" media entity.
   *
   *   This method processes "Acquia DAM" image URLs containing a "mid"
   *   query parameter.
   *   It attempts to load the associated media entity and renders
   *   the image field based on the provided parameters.
   *
   * @param string $selector
   *   The image selector string that includes the image URL.
   * @param string|null $style
   *   (Optional) The image style to use for rendering.
   * @param array $attributes
   *   (Optional) An array of attributes to add to the image element.
   * @param bool $responsive
   *   (Optional) Whether the image should be rendered as a responsive image.
   * @param bool $check_access
   *   (Optional) Whether to perform access checks.
   *
   * @return array
   *   A render array based on the specified settings. Returns an empty array
   *   if the media entity or its relevant field is not accessible.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public static function drupalImage(string $selector, string $style = NULL, array $attributes = [], bool $responsive = FALSE, bool $check_access = TRUE): array {
    // All "Acquia DAM" image urls should contain "mid" query parameter.
    /* @see \Drupal\ieee_twig_helper\IeeeTwigHelper::getMediaUrl() */
    $parts = UrlHelper::parse($selector);
    $mid = $parts['query']['mid'] ?? NULL;
    if (!$mid) {
      return parent::drupalImage($selector, $style, $attributes, $responsive, $check_access);
    }

    $media = \Drupal::entityTypeManager()
      ->getStorage('media')
      ->load($mid);

    if (!$media instanceof MediaInterface || $media->bundle() !== 'acquia_dam_image_asset') {
      return [];
    }

    // This field contains all necessary data for rendering the image.
    if ($media->get(MediaSourceField::SOURCE_FIELD_NAME)->isEmpty()) {
      return [];
    }

    // The "Acquia DAM" media entity always contains only one item, so we can
    // forget about given url in "$selector" variable and just render the
    // field with necessary formatter settings.
    /** @var \Drupal\acquia_dam\Plugin\Field\FieldType\AssetItem $field */
    $field = $media->get(MediaSourceField::SOURCE_FIELD_NAME)->first();

    // Formatter settings for the field item.
    $formatter_setting = $responsive
      ? [
        'type' => 'acquia_dam_responsive_image',
        'label' => 'hidden',
        'settings' => [
          'responsive_image_style' => $style ?? 'original',
        ],
      ]
      : [
        'type' => 'acquia_dam_embed_code',
        'label' => 'hidden',
        'settings' => [
          'embed_style' => $style ?? 'original',
        ],
      ];

    return $field->view($formatter_setting);
  }

  /**
   * Filters and returns the image URL with a specific style applied.
   *
   *   This method supports "image" file entity and "Acquia DAM" media entity.
   *
   * @param string|null $path
   *   The file path for the image. Should start with 'acquia-dam://'.
   *   If the path does not conform, it delegates to the parent method.
   * @param string $style
   *   The desired image style identifier.
   *
   * @return string|null
   *   The URL of the styled image if found,
   *   or NULL if the path or style is invalid.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function imageStyleFilter(?string $path, string $style): ?string {
    // We shouldn't process other than "Acquia DAM" entities.
    if (!str_starts_with($path, 'acquia-dam://')) {
      return parent::imageStyleFilter($path, $style);
    }

    // All "Acquia DAM" image urls should contain "mid" query parameter.
    /* @see \Drupal\ieee_twig_helper\IeeeTwigHelper::getMediaUri() */
    $parts = UrlHelper::parse($path);
    $mid = $parts['query']['mid'] ?? NULL;
    if (!$mid) {
      trigger_error(sprintf('"mid" parameter is missed in %s.', $path));
      return NULL;
    }

    $media = \Drupal::entityTypeManager()
      ->getStorage('media')
      ->load($mid);

    if (!$media instanceof MediaInterface || $media->bundle() !== 'acquia_dam_image_asset') {
      trigger_error(sprintf('Media entity with %s ID does not exist.', $mid));
      return NULL;
    }

    // Get a required image style url from media entity.
    $image_styles = $media->get(ComputedEmbedCodesField::FIELD_NAME)->getValue()[0] ?? [];

    return $image_styles[$style]['href'] ?? NULL;
  }

}