<?php

declare(strict_types=1);

namespace Drupal\dp_twig_helper_acquia_dam_remote_image;

use Drupal\acquia_dam\Entity\ComputedEmbedCodesField;
use Drupal\acquia_dam\Entity\MediaSourceField;
use Drupal\Component\Utility\UrlHelper;
use Drupal\media\MediaInterface;

/**
 * Provides a helper class for the IEEE Twig Extension.
 */
class IeeeTwigHelper {

  /**
   * Generates and returns a media URL for the given media entity.
   *
   * @param MediaInterface $media
   *   The media entity for which the URL should be generated.
   *
   * @return string
   *   The media URL with the media ID as a query parameter. Returns an empty
   *   string if the media entity's bundle is not 'acquia_dam_image_asset'.
   */
  public static function getMediaUrl(MediaInterface $media): string {
    if ($media->bundle() !== 'acquia_dam_image_asset') {
      return '';
    }

    $image_styles = $media->get(ComputedEmbedCodesField::FIELD_NAME)->getValue()[0] ?? [];
    $url = $image_styles['original']['href'] ?? NULL;
    $url_parts = UrlHelper::parse($url);
    $url_parts['query']['mid'] = $media->id();


    // Add media id to url. This parameter can be used in other functions
    // when need to handle this url.
    return $url_parts['path'] . '?' . UrlHelper::buildQuery($url_parts['query']);
  }

  /**
   * Generates and returns a media URI for the given media entity.
   *
   * @param MediaInterface $media
   *   The media entity for which the URI should be generated.
   *
   * @return string
   *   The media URI with the media ID as a query parameter. Includes version ID
   *   if the media entity is not the latest revision. Returns an empty string
   *   if the media entity's bundle is not 'acquia_dam_image_asset'.
   */
  public static function getMediaUri(MediaInterface $media): string {
    if ($media->bundle() !== 'acquia_dam_image_asset') {
      return '';
    }

    $asset_field = $media->get(MediaSourceField::SOURCE_FIELD_NAME);

    $uri = "acquia-dam://$asset_field->asset_id";
    if (!$media->isLatestRevision()) {
      $uri .= "/$asset_field->version_id";
    }

    // Add media id to uri. This parameter can be used in other functions
    // when need to handle this uri.
    $uri .= '?mid=' . $media->id();

    return $uri;
  }

}
